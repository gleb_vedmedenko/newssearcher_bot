﻿namespace LuisBot.Model.News
{
    public class News
    {
        public INewable newsType;
        
        public News(INewable newsType)
        {
            this.newsType = newsType;
        }

    }
}