﻿using System;

namespace LuisBot.Model.News
{
    [Serializable]    
    public class NewsSearcher: INewable
    {

        public string URL => "http://news.google.com/news/rss/search/section/q/";

        public string CreateRequest(string input)
        {
            return URL + input + "/" + input + "?hl=en&output=rss";
        }
    }
}