﻿using System;

namespace LuisBot.Model.News
{
    [Serializable]
    public class NewsSelector: INewable
    {
        public string URL => "https://news.google.com/news/rss/headlines/section/topic/";

        public string CreateRequest(string input)
        {
            return URL + input;
        }
    }
}