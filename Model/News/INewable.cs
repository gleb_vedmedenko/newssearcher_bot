﻿
namespace LuisBot.Model.News
{
   public interface INewable
    {
        string URL { get; }
        string CreateRequest(string input);
    }
}
