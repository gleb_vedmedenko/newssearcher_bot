﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuisBot.Model.News
{
    [Serializable]
    public class DataNews
    {
        public string Title { get; set; }
        public string Link{ get; set; }
        public string ItemId { get; set; }
        public string PubDate { get; set; }
        public string Description { get; set; }      
    }
}