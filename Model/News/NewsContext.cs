﻿using System.Collections.Generic;

namespace LuisBot.Model.News
{
    public static class NewsContext
    {
        public readonly static INewable searcher = new News(new NewsSearcher()).newsType;
        public readonly static INewable selector = new News(new NewsSelector()).newsType;

        public static int Count => dataList.Count;
        public static int Capacity { get; set; }

        public static List<DataNews> dataList = new List<DataNews>();
        public static Dictionary<string, DataNews> items = new Dictionary<string, DataNews>();

    }
}