﻿using System;
using System.Linq;
using System.Collections.Generic;
using LuisBot.Model.News;
namespace LuisBot.Model.User
{

    [Serializable]
    public class User
    {

        public DateTime UpdateTime { get; set; }
        public UserConfiguration UserConfiguration { get; private set; }
        public UserOption currentOption { get; set; }
        public List<DataNews> userNews = new List<DataNews>();

        public User(UserConfiguration configuration)
        {
            UserConfiguration = configuration;
        }

        public void AddOption(UserOption option)
        {
            UserConfiguration.AddOption(option);
        }

        public UserOption GetOption(string key)
        {
            return UserConfiguration.userOptions.FirstOrDefault(x => x.Key.Equals(key));
        }

        public void RemoveOption(UserOption option)
        {
            UserConfiguration.RemoveOption(option);
        }

        public void ClearUserKeys(UserOption option)
        {
            UserConfiguration.RemoveOption(option);
        }
        

    }
}