﻿using System;
using System.Collections.Generic;
using LuisBot.Model.News;
using LuisBot.Model;
namespace LuisBot.Model.User
{
    [Serializable]
    public class UserOption
    {

        public int Capacity { get; private set; }
        public string Key { get; private set; }
        public Enums.UserOption option = new Enums.UserOption();

        public UserOption(string key, int capacity, Enums.UserOption userOption)
        {
            Key = key;
            Capacity = capacity;
            option = userOption;
        }

        public void SetCapacity(int newValue)
        {
            Capacity = newValue;
        }

        public void SetOption(string newKey)
        {
            Key = newKey;
        }
        
    }
}