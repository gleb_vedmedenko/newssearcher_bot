﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuisBot.Model.User
{
    [Serializable]
    public static class UserContext
    {
        public static  Dictionary<string, User> userNewsData = new Dictionary<string, User>();
        public static User currentUser = null;
        public static UserOption currentOption = null;
    }
}