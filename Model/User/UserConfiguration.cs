﻿using System.Collections.Generic;
using System;

namespace LuisBot.Model.User
{
    [Serializable]
    public class UserConfiguration
    {
        
        public List<UserOption> userOptions = new List<UserOption>();
        public string UserID { get; private set; }

        public UserConfiguration(string id)
        {
            UserID = id;
        }

        public void AddOption(UserOption option)
        {
            userOptions.Add(option);
        }

        public void RemoveOption(UserOption option)
        {
            userOptions.Remove(option);
        }

        public void ClearOptions(UserOption option)
        {
            userOptions.Clear();
        }
        
    }
}