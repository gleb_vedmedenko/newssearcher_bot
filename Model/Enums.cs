﻿using System;

namespace LuisBot.Model
{
    [Serializable]
    public static class Enums
    {
        
            public enum InterestOptions
            {
                NewsType,
                Search
            }
            
            public enum TypeOptions
            {
                World,
                Business,
                Technology,
                Entertainment,
                Sports,
                Science,
                Health,
                Other
            }
            
            public enum OptionType
            {
                Show_more,
                Other
            }
            public enum UserOption
            {
                UserOption,
                DefaultOption
            }
        }
    }
