﻿using System;
using LuisBot.Model.News;

namespace LuisBot.Controllers
{
    public class NewsController
    {
        public static void RemoveItem(DataNews data)
        {
            NewsContext.dataList.Remove(data);
        }
        public static void AddData(DataNews data)
        {
            NewsContext.dataList.Add(data);
        }
        public static void ClearData()
        {
            NewsContext.dataList.Clear();
        }
    }
}