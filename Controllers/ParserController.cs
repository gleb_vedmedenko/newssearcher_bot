﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
using System.Data;
using LuisBot.Model.News;
using LuisBot.Model.User;

namespace LuisBot.Controllers
{
    public static class ParserController
    {

        public static void Parse(INewable newsType,  User user, string input)
        {
            // httpWebRequest with API URL
            var request = (HttpWebRequest)WebRequest.Create
            (newsType.CreateRequest(input));

            //Method GET
            request.Method = "GET";

            //HttpWebResponse for result
            var response = (HttpWebResponse)request.GetResponse();


            //Mapping of status code
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (response.CharacterSet == "")
                    readStream = new StreamReader(receiveStream);
                else
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));

                var data = readStream.ReadToEnd();
                var ds = new DataSet();
                var reader = new StringReader(data);
                ds.ReadXml(reader);
                var dtGetNews = new DataTable();

                if (ds.Tables.Count > 3)
                {
                    dtGetNews = ds.Tables["item"];
                    foreach (DataRow dtRow in dtGetNews.Rows)
                    {
                        var DataObj = new DataNews();
                        DataObj.Title = dtRow["title"].ToString();
                        DataObj.Link = dtRow["link"].ToString();
                        DataObj.ItemId = dtRow["item_id"].ToString();
                        DataObj.PubDate = dtRow["pubDate"].ToString();
                        DataObj.Description = dtRow["description"].ToString();
                        user.userNews.Add(DataObj);
                    }
                }
            }
        }
    }
}
