﻿using LuisBot.Model.User;
using LuisBot.Model;
namespace LuisBot.Controllers
{
    public static class UserController
    {
        // 1. Create user configuration
        public static UserConfiguration CreateConfiguration(string id)
        {
            return new UserConfiguration(id);
        }
        // 2. Create user option as much as you want
        public static UserOption CreateOption(string key, int capacity)
        {
            return null;
            //return new UserOption(key, capacity);
        }
        // 3. Add option to configuration
        public static void AddUserOption(User user, UserOption option)
        {
            UserContext.currentOption = option;
            user.UserConfiguration.AddOption(option);
        }
        // 4. Create user and initialize configuration
        public static void CreateUser(UserConfiguration configuration)
        {
            var user = new User(configuration);
            AddNewUser(user, user.UserConfiguration.UserID);

        }

        public static void AddNewUser(User user, string id)
        {
            UserContext.userNewsData.Add(id, user);
           // UserContext.currentUser = user;
        }

        public static User GetCurrentUser()
        {
            return UserContext.currentUser;
            //User user;
            //UserContext.userNewsData.TryGetValue(key, out user);
            //return user;
        }

        public static void SetCurrentUser(string id)
        {
            User user;
            UserContext.userNewsData.TryGetValue(id, out user);
            UserContext.currentUser = user;
        }

        public static bool isContainsUser(string id)
        {
            return UserContext.userNewsData.ContainsKey(id);
        }

        public static User GetUserByID (string id)
        {
            User user;
            UserContext.userNewsData.TryGetValue(id, out user);
            return user;
        }

        public static void AddNewUserOption(User user,string key,Enums.UserOption userOption)
        {
            var option = new UserOption(key, 2, userOption);
            user.AddOption(option);
            user.currentOption = option;
            
        }
        public static UserOption GetCurrentOption()
        {
            return UserContext.currentOption;
        }

        public static void CreateUserOption(string key)
        {
          //  UserContext.currentOption =  new UserOption(key, 2); //default capacity
        }
    }
}