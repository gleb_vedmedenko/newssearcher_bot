using System;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Dialogs;
using System.Net.Http;
using System.Collections.Generic;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using LuisBot.Model.News;
using LuisBot.Model;
using LuisBot.Model.User;
using LuisBot.Controllers;

namespace Microsoft.Bot.Sample.LuisBot
{
    // For more information about this template visit http://aka.ms/azurebots-csharp-luis
    [LuisModel("20a377b4-224f-4b7d-9f98-417cd2aaa4df", "56e6b3a274cc4d019a3eb565748617c1")]

    [Serializable]
    public class BasicLuisDialog : LuisDialog<object>
    {
        
        User user;      
        [LuisIntent("None")]
        public async Task NoneIntent(IDialogContext context, LuisResult result)
        {
            await this.ShowLuisResult(context, result);
        }

        // Go to https://luis.ai and create a new intent, then train/publish your luis app.
        // Finally replace "Gretting" with the name of your newly created intent in the following handler
        [LuisIntent("Greeting")]
        public async Task GreetingIntent(IDialogContext context, LuisResult result)
        {
            if (UserController.isContainsUser(context.Activity.From.Id)) user = UserController.GetUserByID(context.Activity.From.Id);
            else UserController.AddNewUser(new User(new UserConfiguration(context.Activity.From.Id)), context.Activity.From.Id);
            //  NewsContext.Capacity = 3;
            // await this.ShowLuisResult(context, result);
            // await context.PostAsync("Hello what are you intresting for? Input any key word or type of news");
            //context.Wait(MessageReceivedAsync);
            //  PromptDialog.Text(context, TypeMessage, "What are you intresting for?");
            PromptDialog.Choice(
    context,
    UserSetting,
    (IEnumerable<Enums.InterestOptions>)Enum.GetValues(typeof(Enums.InterestOptions)),
    "What are you intresting for ? ",
    promptStyle: PromptStyle.Auto);
        }

        [LuisIntent("Cancel")]
        public async Task CancelIntent(IDialogContext context, LuisResult result)
        {
            //await this.ShowLuisResult(context, result);
            NewsContext.Capacity = 3;
            PromptDialog.Choice(
                    context,
                    GetTypeNews,
                    (IEnumerable<Enums.TypeOptions>)Enum.GetValues(typeof(Enums.TypeOptions)),
                    "What are you intresting for ? ",
                    promptStyle: PromptStyle.Auto);
            //PromptDialog.Choice(
            //        context,
            //        ResumeGetDescription,
            //        (IEnumerable<Enums.InterestOptions>)Enum.GetValues(typeof(Enums.InterestOptions)),
            //        "Chose options",
            //        promptStyle: PromptStyle.Auto);

        }

        [LuisIntent("Search")]
        public async Task HelpIntent(IDialogContext context,LuisResult result)
        {
            NewsContext.Capacity = 3;
            PromptDialog.Choice(
                    context,
                    GetTypeNews,
                    (IEnumerable<Enums.TypeOptions>)Enum.GetValues(typeof(Enums.TypeOptions)),
                    "What are you intresting for ? ",
                    promptStyle: PromptStyle.Auto);
           // await this.ShowLuisResult(context, result);
        }

        private async Task ShowLuisResult(IDialogContext context,LuisResult result) 
        {
            await context.PostAsync($"You have some reached  query"+ " You said: hola");
            context.Wait(MessageReceived);
        }

        public async Task GetTypeNews(IDialogContext context, IAwaitable<Enums.TypeOptions> option)
        {
            var type = await option;
            var selector = NewsContext.selector;
            switch (type)
            {
                //case Enums.TypeOptions.Sports:
                //    ParserController.Parse(selector,("SPORTS"));
                //    break;
                //case Enums.TypeOptions.Science:
                //    ParserController.Parse(selector, ("SCIENCE"));
                //    break;
                //case Enums.TypeOptions.Technology:
                //    ParserController.Parse(selector, ("TECHNOLOGY"));
                //    break;
                //case Enums.TypeOptions.World:
                //    ParserController.Parse(selector, ("WORLD"));
                //    break;
                //case Enums.TypeOptions.Business:
                //    ParserController.Parse(selector, ("BUSINESS"));
                //    break;
                //case Enums.TypeOptions.Entertainment:
                //    ParserController.Parse(selector, ("ENTERTAINMENT"));
                //    break;
                //case Enums.TypeOptions.Health:
                //    ParserController.Parse(selector, ("HEALTH"));
                //    break;
            }
            await ShowNews(context);
            //PromptDialog.Choice(
            //    context,
            //    GetMessageText,
            //    (IEnumerable<OptionType>)Enum.GetValues(typeof(OptionType)),
            //    "Show  me"
            //    );
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> argument)
        {
            var message = await argument;
            
            await context.PostAsync("You choose " + message.From.Name + " " + message.Text);
            await MessageReceived(context, argument);
            //if (message.Text == "reset")
            //{
            //    PromptDialog.Confirm(
            //        context,
            //        AfterResetAsync,
            //        "Are you sure you want to reset the count?",
            //        "Didn't get that!",
            //        promptStyle: PromptStyle.Auto);
            //}
            //else
            //{
            //PromptDialog.Choice(
            //        context,
            //        ResumeGetDescription,
            //        (IEnumerable<Enums.InterestOptions>)Enum.GetValues(typeof(Enums.InterestOptions)),
            //        "Chose options",
            //        promptStyle: PromptStyle.Auto);
            //await context.PostAsync($"{this.count++}: You said {message.Text}");
            //    context.Wait(MessageReceivedAsync);
            //}
        }
        
        public async Task ResumeGetDescription(IDialogContext context, IAwaitable<Enums.InterestOptions> result)
        {
            var description = await result;

            if (description == Enums.InterestOptions.Search)
            {
                  PromptDialog.Text(context, TypeMessage, "What are you intresting?");

            }
            else if( description == Enums.InterestOptions.NewsType)
            {
                PromptDialog.Choice(
                    context,
                    GetTypeNews,
                    (IEnumerable<Enums.TypeOptions>)Enum.GetValues(typeof(Enums.TypeOptions)),
                    "What are you intresting for ? ",
                    promptStyle: PromptStyle.Auto);
            }
            //PromptDialog.Confirm(
            //context: context,
            //resume: AfterResetAsync,
            //prompt: $"You entered Product :- '{description}'",
            //retry: "I didn't understand. Please try again.");
        }

        public async Task UserOptions(IDialogContext context, IAwaitable<Enums.InterestOptions> result)
        {
            var description = await result;
            //if (UserController.isContainsUser(context.Activity.From.Id)) user = UserController.GetUserByID(context.Activity.From.Id);
            //else user = new User(new UserConfiguration(context.Activity.From.Id));
            //if (description == Enums.InterestOptions.Search)
            //{
            //    PromptDialog.Text(context, AddKey, "Type key");   
            //}
            //if( description == Enums.InterestOptions.NewsType)
            //{

            //}
            PromptDialog.Choice(context,
                GetCurrentOption,
                (IEnumerable<Enums.TypeOptions>)Enum.GetValues(typeof(Enums.TypeOptions)),
                "What are you intresting for ? ",
                    promptStyle: PromptStyle.Auto
                );
        }

        public async Task GetCurrentOption(IDialogContext context, IAwaitable<Enums.TypeOptions> types)
        {
            var options = await types;
            switch (options)
            {
                case Enums.TypeOptions.Sports:
                    UserController.AddNewUserOption(UserController.GetUserByID(context.Activity.From.Id),"SPORTS",Enums.UserOption.DefaultOption);
                    PromptDialog.Text(context, AddCapacity, " How many news you want to watch ?");
                    break;
                case Enums.TypeOptions.Science:
                    UserController.AddNewUserOption(UserController.GetUserByID(context.Activity.From.Id), "SCIENCE", Enums.UserOption.DefaultOption);
                    PromptDialog.Text(context, AddCapacity, " How many news you want to watch ?");
                    break;
                case Enums.TypeOptions.Technology:
                    UserController.AddNewUserOption(UserController.GetUserByID(context.Activity.From.Id), "TECHNOLOGY", Enums.UserOption.DefaultOption);
                    PromptDialog.Text(context, AddCapacity, " How many news you want to watch ?");
                    break;
                case Enums.TypeOptions.World:
                    UserController.AddNewUserOption(UserController.GetUserByID(context.Activity.From.Id), "WORLD", Enums.UserOption.DefaultOption);
                    PromptDialog.Text(context, AddCapacity, " How many news you want to watch ?");
                    break;
                case Enums.TypeOptions.Business:
                    UserController.AddNewUserOption(UserController.GetUserByID(context.Activity.From.Id), "BUSINESS", Enums.UserOption.DefaultOption);
                    PromptDialog.Text(context, AddCapacity, " How many news you want to watch ?");
                    break;
                case Enums.TypeOptions.Entertainment:
                    UserController.AddNewUserOption(UserController.GetUserByID(context.Activity.From.Id), "ENTERTAINMENT", Enums.UserOption.DefaultOption);
                    PromptDialog.Text(context, AddCapacity, " How many news you want to watch ?");
                    break;
                case Enums.TypeOptions.Health:
                    UserController.AddNewUserOption(UserController.GetUserByID(context.Activity.From.Id), "HEALTH", Enums.UserOption.DefaultOption);
                    PromptDialog.Text(context, AddCapacity, " How many news you want to watch ?");
                    break;
                case Enums.TypeOptions.Other:
                    //UserController.AddNewUserOption(UserController.GetUserByID(context.Activity.From.Id), "WORLD", Enums.UserOption.DefaultOption);
                    break;
            }
        }



        public async Task AddKey(IDialogContext context, IAwaitable<string> result)
        {
            var description = await result;
            //UserController.CreateUserOption(description);
            
            PromptDialog.Text(context, AddCapacity, " How many news you want to watch ?");
        }
        public async Task AddCapacity(IDialogContext context, IAwaitable<string> result)
        {
            var description = await result;
            int m;
            int.TryParse(description, out m);
            //UserController.GetCurrentOption().SetCapacity(m);
            //var user = UserController.GetCurrentUser();
            //user.currentOption.SetCapacity(m);
            //user.UserConfiguration.AddOption(user.currentOption);
            //UserController.AddUserOption(user, UserController.GetCurrentOption());  
            var search = NewsContext.searcher;
            NewsContext.Capacity = m;
            // ParserController.Parse(search, user.currentOption.Key);
            var user = UserController.GetUserByID(context.Activity.From.Id);
            ParserController.Parse(search, user, user.currentOption.Key);
            await context.PostAsync(user.currentOption.Key);
            await context.PostAsync(user.userNews.Count.ToString());
            await ShowNews(context);
           // await context.PostAsync(UserController.GetCurrentOption().Capacity.ToString());
            

        }
        public async Task UserSetting(IDialogContext context, IAwaitable<Enums.InterestOptions> result)
        {
            var description = await result;
           // var configuration = UserController.CreateConfiguration(context.Activity.From.Id);

            //if (UserController.isContainsUser(context.Activity.From.Id))
            //    UserController.SetCurrentUser(context.Activity.From.Id);
            //else
            //    UserController.CreateUser(configuration);
             
            PromptDialog.Choice(
                    context,
                    UserOptions,
                    (IEnumerable<Enums.InterestOptions>)Enum.GetValues(typeof(Enums.InterestOptions)),
                    "What are you intresting for ? ",
                    promptStyle: PromptStyle.Auto);
            
            //if (description == Enums.InterestOptions.Search)
            //{
            //    PromptDialog.Text(context, TypeMessage, "What are you intresting?");

            //}
            //else if (description == Enums.InterestOptions.NewsType)
            //{
            //    PromptDialog.Choice(
            //        context,
            //        GetTypeNews,
            //        (IEnumerable<Enums.TypeOptions>)Enum.GetValues(typeof(Enums.TypeOptions)),
            //        "What are you intresting for ? ",
            //        promptStyle: PromptStyle.Auto);
            //}
            //PromptDialog.Confirm(
            //context: context,
            //resume: AfterResetAsync,
            //prompt: $"You entered Product :- '{description}'",
            //retry: "I didn't understand. Please try again.");
        }

        public async Task TypeMessage(IDialogContext context, IAwaitable<string> message)
        {
            var text = await message;
           // ParserController.Parse(NewsContext.searcher, text);
            
            await context.PostAsync("Here what's we found" + NewsContext.dataList.Count);
            await ShowNews(context);
            // context.Wait(MessageReceivedAsync);
        }

        public async Task ShowNews(IDialogContext context)
        {
            if (UserController.GetUserByID(context.Activity.From.Id).userNews.Count <= 0)
            {
                await context.PostAsync("That's all news for this moment");
                context.Wait(MessageReceivedAsync);
            }
            else
            {
                for (int i = 0; i < NewsContext.Capacity; i++)
                {
                    if (UserController.GetUserByID(context.Activity.From.Id).userNews.Count <= i) break;
                    await context.PostAsync(UserController.GetUserByID(context.Activity.From.Id).userNews[i].Link);
                    UserController.GetUserByID(context.Activity.From.Id).userNews.RemoveAt(i);

                }
                await context.PostAsync(UserController.GetUserByID(context.Activity.From.Id).userNews.Count.ToString());
                await context.PostAsync(UserController.GetUserByID(context.Activity.From.Id).UserConfiguration.UserID);
                PromptDialog.Choice(context,
                    GetMessageText,
                    (IEnumerable<Enums.OptionType>)Enum.GetValues(typeof(Enums.OptionType)),
                    "Show more"
                    );
            }
        }

        public async Task GetMessageText(IDialogContext context, IAwaitable<Enums.OptionType> message)
        {
            var action = await message;
            if (action == Enums.OptionType.Other)
            {
                NewsContext.dataList.Clear();
                context.Wait(MessageReceivedAsync);
            }
            else
                await ShowNews(context);
        }
    }
}